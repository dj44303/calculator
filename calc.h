#ifndef _calc_h
#define _calc_h


//Function returns element from char[] starting on int* position
double getvar(char[], int*);

//Function gets two arrays of type double and pass the values of second array to first
void rewritedouble(double*, double*);

//Function gets two arrays of type char and pass the values of second array to first
void rewritechar(char*, char*);

//Function takes arrays of double values, char math operators, integer position of operation and integer size of char array
//It does an operation using operator on n position of char array, and two numbers on n, n+1 positions of double array,
//then removes operator from array and move other numbers in double array
void calculate(double*, char*, int, int);


#endif // _calc_h
