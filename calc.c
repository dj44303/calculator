#include <stdio.h>
#include <math.h>
#include "calc.h"

double getvar(char temp[], int *c){
    char ret[128];
    int r = 0;
    double retint = 0;
    while(temp[*c] && temp[*c] != ' ' && temp[*c] != '\n'){
        ret[r] = temp[*c];
        r++;
        *c = *c + 1;
        if(ret[r-1] < '0' || ret[r-1] > '9') break;
    }
    *c = *c + 1;
    if(ret[0] >= '0' && ret[0] <= '9'){
        for(int i = r-1; i >= 0; i--){
            retint = retint + (pow(10,r-i-1) * ((int)ret[i] - 48));
        }
        return retint;
    }
    else {
        return ret[0];
    }
}

void rewritedouble(double *tab1, double *tab2){
    for(int i = 0; i < sizeof(tab2); i++)
        tab1[i] = tab2[i];
}

void rewritechar(char *tab1, char *tab2){
    for(int i = 0; i < sizeof(tab2); i++)
        tab1[i] = tab2[i];
}

void calculate(double *values, char *calc, int j, int siz){
    double temp[50];
    char temp2[50];
    for(int i = 0; i < siz; i++){
        if(i == j){
            switch((int)calc[j]){
            case '+':
                temp[i] = values[i] + values[i+1];
                break;
            case '-':
                temp[i] = values[i] - values[i+1];
                break;
            case '*':
                temp[i] = values[i] * values[i+1];
                break;
            case '/':
                temp[i] = values[i] / values[i+1];
                break;
            }
        }
        else if(i > j){
            temp[i] = values[i+1];
            temp2[i-1] = calc[i];
        } else {
            temp[i] = values[i];
            temp2[i] = calc[i];
        }
    }
    rewritedouble(values, temp);
    rewritechar(calc, temp2);
}

int main(){

    int l = 0;
    size_t bufsize = 128;
    char voidx1[128];


    printf("Liczba wyrazen: ");
    scanf("%i", &l);
    double res[l];
    fgets(voidx1, bufsize, stdin);
    remove(voidx1);

    for(int i = 0 ; i < l ; i++){
        int c = 0, siz = 0;
        char line[128], calc[50];;
        double values[50];

        fgets(line, bufsize, stdin);
        values[0] = getvar(line, &c);

        for(siz = 0; line[c] != '\0' && line[c]; siz++){
            calc[siz] = getvar(line, &c);
            values[siz+1] = getvar(line, &c);
        }

        while(siz){
            for(int j = 0; j < siz; j++){
                if(calc[j] == '*' || calc[j] == '/'){
                    calculate(values, calc, j, siz);
                    siz--, j--;
                }
            }

            for(int j = 0; j < siz; j++){
                if(calc[j] == '+' || calc[j] == '-'){
                    calculate(values, calc, j, siz);
                    siz--, j--;
                }
            }
        }

        res[i] = values[0];


    }


    for(int i = 0; i < l; i++)
        if(fmod(res[i], 1.0))
            printf("\n%.1f", floor(res[i]*10)/10);
            //printf("\n%.1f", res[i]);
        else
            printf("\n%.0f", res[i]);
    printf("\n");
    return 0;
}
